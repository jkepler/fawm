var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");

$(function() {

    var defaultTags = ["acapella","acoustic","acoustic-one-take","acoustic-rock","alt-country","alt-rock","alternative","alternative-rock","ambient","americana","ballad","banjo","bass","bluegrass","blues","children","chill","chiptune","christian","classical","collab","comedy","country","country-folk","cowboy","crucio","dance","dark","demo","downtempo","drone","drum-n-bass","electronic","electronica","electropop","emo","experimental","explore-the-core","exquisite-corpse","fast","fawmcast","fawmtronica","feast","filk","firstfruits","folk","folk-pop","folk-rock","folky","fuc","funk","funny","geek-rock","girl-with-guitar","girl-with-piano","girl-with-ukulele","gospel","guitar","guitars","guy-with-guitar","guy-with-piano","guy-with-ukulele","happy","hard-rock","harmonies","heavy","hip-hop","humor","improvisation","indie","indie-folk","indie-pop","indie-rap","indie-rock","industrial","instrumental","jazz","lo-fi","love","love-song","lyrics","lyrics-only","major-key","mandolin","melancholy","metal","minor-key","mokey-rock","music","needs","needs-collab","needs-lyrics","needs-music","needs-vocals","new-wave","noise","non-english","odd-instrument","odd-time-signature","one-take","orchestral","piano","poetry","political","pop","pop-punk","pop-rock","progressive","protest","psychedelic","punk","rap","reggae","retro","rhythm-n-blues","rnb","rock","sad","sci-fi","silly","singer-songwriter","slow","solo","songskirmish","soul","soundscape","soundtrack","space","space-rock","spiritual","spoken-word","story","strangle-disco","superskirmish","synth","synthpop","techno","thrash","trance","ukulele","underground","upbeat","urban","valentine","vocals","waltz","weird","western","worship","youtube","vimeo","soundcloud","nebulus"];

    var userRefs = [];

    // enable tabs
    $('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('.select-nav').change(function() {
        if ($(this).val() !== '') {
            window.location.href=$(this).val();
        }
    });

    // setup dropdown menu
    $('.dropdown-toggle').dropdown();

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top-69
        }, 750, 'swing');
        event.preventDefault();
    });

    // wrapper for anything that involves localStorage
    function localStorageCheck(targetFunction) {
        if (typeof localStorage === 'object') {
            try {
                targetFunction();
            } catch (e) {
                alert('It looks like you are using a browser in "Private Browsing" mode. Be aware that some of FAWM\'s features may not work properly for you.');
            }
        }
    }

    // append usernames on this page to HTML5 local storage
    localStorageCheck(function() {
        if (localStorage.getItem("user-refs") !== null) {
            userRefs = JSON.parse(localStorage["user-refs"]);
        }
    });
    $('a.user-ref').each(function(i, el) {
        var username = $(this).text().trim();
        if ($.inArray(username, userRefs) === -1) userRefs.push(username);
    });


    // lowercase usernames
    $('#username').blur(function (e) {
        $(this).val($(this).val().toLowerCase());
    });

    // override jquery validate plugin defaults
    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    // autocomplete & highlighting for usernames
    $('textarea').textcomplete([
        { // usernames
            userRefs: userRefs,
            match: /\B@(\w*)$/,
            search: function (term, callback) {
                callback($.map(this.userRefs, function (username) {
                    return username.indexOf(term) === 0 ? username : null;
                }));
            },
            index: 1,
            replace: function (username) {
                return '@' + username + ' ';
            }
        },
        { // tags
            tagRefs: defaultTags,
            match: /\B#([\w-_]*)$/,
            search: function (term, callback) {
                callback($.map(this.tagRefs, function (theTag) {
                    return theTag.indexOf(term) === 0 ? theTag : null;
                }));
            },
            index: 1,
            replace: function (theTag) {
                return '#' + theTag + ' ';
            }
        }
        ], { zIndex: 3000, maxCount: 14, className: 'textcomplete-dropdown' }).overlay([
        {
            match: /\B[@#][\w-_]+/g,
            css: {
                'background-color': '#ffcc99'
            }
        }
    ]);

    // autocomplete & highlighting for tags
    $('#tags').textcomplete([
        { // html
            tagRefs: defaultTags,
            match: /\b([\w-_]+)$/,
            search: function (term, callback) {
                callback($.map(this.tagRefs, function (theTag) {
                    return theTag.indexOf(term) > -1 ? theTag : null;
                }));
            },
            index: 0,
            replace: function (theTag) {
                return theTag + ' ';
            }
        }
        ], { appendTo: 'body' }).overlay([
        {
            match: /\b[\w-_]+/g,
            css: {
                'background-color': '#ffcc99',
            }
        }
    ]);


    $.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z][a-z0-9]+$/i.test(value);
    }, "Must be a letter followed by letters &amp; numbers.");

    $.validator.addMethod("emailsMatch", function(value) {
      return $('#email').val() == $('#email2').val();
    }, "Be sure the email address matches.");

    $.validator.addMethod("passwordsMatch", function(value) {
      return $('#pass1').val() == $('#pass2').val();
    }, "Be sure the password matches.");
    $.validator.addMethod("easyURL", function(value, element) {
        return this.optional( element ) || /^((https?|s?ftp):\/\/)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test( value );
    }, "Please enter a valid URL.");

    // auth form validation
    $('.auth-form').validate({
        rules: {
            username: {
                minlength: 3,
                loginRegex: true,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            email2: {
                required: true,
                emailsMatch: true,
                email: true
            }
        },
    });

    $('.profile-settings-form').validate({
        rules: {
            nickname: {
                minlength: 2,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            url: {
                easyURL: true
            },
            pass2: {
                passwordsMatch: true
            }
        },
    });

    $('.song-edit-form').validate({
        rules: {
            title: {
                required: true
            },
            demo: {
                easyURL: true
            },
        },
    });

    $('.new-forum-topic-form').validate({
        rules: {
            title: {
                minlength: 3,
                required: true
            }
        },
    });

    // Autoplay/auto-advance songs
    localStorageCheck(function() {
        $("#player").jPlayer({
            ready: function () {
                var player = $(this).jPlayer("setMedia", {
                    mp3: $(this).data("mp3Url")
                });
                localStorage.autoplay && player.jPlayer("play");
            },
            ended: function() {
                if (localStorage.autoadvance) {
                    location.href = "/songs/advance/";
                }
            },
            // supplied: "oga, mp3",
            preload: "auto",
            supplied: "mp3",
            swfPath: "/js/",
            solution: "flash,html"
        });
        $(document).ready(function() {
            var autoplay = localStorage.autoplay;
            var autoadvance = localStorage.autoadvance;

            // Update checkboxes
            var playCheckbox = $(".autoplay");
            if (playCheckbox.length) {
                var advanceCheckbox = $(".autoadvance");

                // Stop if no demo found
                if (!$(".embed-container, .jp-jplayer").length) {
                    playCheckbox.closest("div").hide();
                    return;
                }

                playCheckbox.prop("checked", autoplay).on("change", function() {
                    if (this.checked) {
                        localStorage.autoplay = true;
                    } else {
                        delete localStorage.autoplay;
                    }
                });

                advanceCheckbox.prop("checked", autoadvance).on("change", function() {
                    if (this.checked) {
                        localStorage.autoadvance = true;
                    } else {
                        delete localStorage.autoadvance;
                    }
                });
            } else {
                return;
            }

            // Vimeo
            var vimeoPlayer = $(".vimeo-player");
            if (vimeoPlayer.length) {
                var iframe = vimeoPlayer.find("iframe");
                var src = iframe.attr("data-src");
                iframe.attr("src", autoplay ? src+'amp;autoplay=1' : src);

                var player = vimeoPlayer.find("iframe");
                var url = player.attr("src").split("?")[0];

                // Listen for messages from the player
                if (window.addEventListener){
                    window.addEventListener("message", onMessageReceived, false);
                }
                else {
                    window.attachEvent("onmessage", onMessageReceived, false);
                }

                // Handle messages received from the player
                function onMessageReceived(e) {
                    var data = JSON.parse(e.data);

                    switch (data.event) {
                        case "ready":
                            post("addEventListener", "finish");
                            break;

                        case "finish":
                            if (localStorage.autoadvance) {
                                location.href = "/songs/advance/";
                            }
                            break;
                    }
                }

                // Helper function for sending a message to the player
                function post(action, value) {
                    var data = {
                      method: action
                    };

                    if (value) {
                        data.value = value;
                    }

                    var message = JSON.stringify(data);
                    player[0].contentWindow.postMessage(data, url);
                }

                return;
            }
        });
    });

    // Fix input element click problem
    $('.dropdown input, .dropdown label').click(function(e) {
      e.stopPropagation();
    });

    // activate tabs
    $('#my-sub-nav a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    });


    // for swapping our form fields
    swapValues = [];
    $(".swap_value").each(function(i) {
        swapValues[i] = $(this).val();
        $(this).focus(function(){
            if ($(this).val() == swapValues[i]) {
                $(this).val("");
            }
        }).blur(function(){
            if ($.trim($(this).val()) == "") {
                $(this).val(swapValues[i]);
            }
        });
    });

    // for AJAX commenting
    $('.song-comment-form').submit(function(e) {
        var $self = $(this);
        $.post(
            $self.attr('action'),
            $self.serialize(),
            function(html) {
                $(html).hide().insertAfter("#comment-form-item").fadeIn(400);
                $('li.media').hover(function() {
                    $(this).find('.media-meta').css('visibility', 'visible');
                }, function() {
                    $(this).find('.media-meta').css('visibility', 'hidden');
                });
                $self.find('textarea').val('');
            }
        );
        return false;
    });

    // alternate AJAX editing
    $(document).on('click', '.modalEdit', function() {
        $('#modalIndicator').show();
        var $self = $(this);
        var modalEdit = $('#modalEdit');
        $.get(
            $self.attr('data-src'),
            {},
            function(data) {
                modalEdit.html(data);
                modalEdit.find('form').submit(function(e) {
                    // $(this).hide();
                    modalContent = modalEdit.find('.modal-content');
                });
                validateAjaxForms();
                modalEdit.modal('show');
            }
        );
    });

    // for AJAX editing
    var editModal = $('#editModal');
    var textarea = editModal.find('textarea');
    var editAvatar = editModal.find('img');

    $(document).on('click', '.editComment', function() {
        var $self = $(this);
        var p = $self.closest('.comment-item').find('p');

        // Prepopulate textarea
        textarea.val(p.text()).focus();
        var content = textarea.val();
        if (textarea.attr('data-limit-input') !== undefined) {
            var charLimit = textarea.attr('data-limit-input');
            content = content.substring(0, textarea.attr('data-limit-input'));
            var count      = textarea.closest('.media-body').find('.count');
            var charLength = content.length,
                charDiff   = charLimit - charLength;
            count.html(charDiff);
        }

        // update avatar
        var targetAvatar = $self.closest('li').find('img');
        editAvatar.attr('src', targetAvatar.attr('src'));
        editAvatar.css('background-color', targetAvatar.css('background-color'));

        // Show modal
        editModal.modal('show');

        // Handle submission
        editModal.find('.btn-primary').one('click', function() {
            $.post($self.attr('data-api'), {
                content: textarea.val().substring(0, 2000)
            }).always(function() {
                $('#editModal').modal('hide');
            }).done(function(data) {
                p.html(data);
            });
        });

        return false;
    });

    // "characters remining" countdown
    $("textarea[data-limit-input]").keyup(function (e) {
        var $self      = $(this),
            charLimit  = $self.attr("data-limit-input");
        if ($self.context.textLength > charLimit)
            $self.context.value = $self.context.value.substring(0, charLimit);
        var count      = $self.closest('.media-body').find('.count');
        var charLength = $self.context.textLength,
            charDiff   = charLimit - charLength;
        count.html(charDiff);
    });

    // enable tooltips
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    // make comment meta stuff (flag/edit/delete/etc.) hide/show on rollover
    $('li.media').hover(function() {
        $(this).find('.media-meta').css('visibility', 'visible');
    },
    function() {
        $(this).find('.media-meta').css('visibility', 'hidden');
    });

    // ajax spam flagging
    $('.flagComment a').click(function(e) {
        e.preventDefault();
        var link = $(this);
        var url = link.attr('href').replace('flag', 'ajax_flag');
        $.post(url, function(result) {
            if (result == 1) {
                link.closest('.flagComment').fadeTo(200, 0.1, function() {
                    link.closest('.flagComment').html('<strong><i class="icon-flag"></i>reported</strong>').fadeTo(500,1);
                });
            }
        });
    });

    // ajax flag removal
    $('.removeFlag a').click(function(e) {
        e.preventDefault();
        var link = $(this);
        var url = link.attr('href').replace('flag', 'ajax_flag');
        $.post(url, function(result) {
            if (result == 1) {
                link.closest('.aComment').slideUp(400);
            }
        });
    });

    // ajax deleting
    $('.deleteComment').click(function(e) {
        // e.preventDefault();
        if(confirm('Are you sure you want to delete this?\n  \'OK\' to delete, \'Cancel\' to stop.')) {
            var link = $(this);
            var url = link.attr('href');
            // alert(url);
            $.post(url, function() {
                link.closest('li.media').slideUp(400);
            });
        }
        return false;
    });

});

}
/*
     FILE ARCHIVED ON 21:17:38 Feb 01, 2021 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 01:03:36 Mar 07, 2021.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 56.774 (3)
  exclusion.robots.policy: 0.164
  captures_list: 103.353
  esindex: 0.022
  PetaboxLoader3.resolve: 56.792
  RedisCDXSource: 26.47
  PetaboxLoader3.datanode: 71.92 (4)
  CDXLines.iter: 16.659 (3)
  load_resource: 112.307
  exclusion.robots: 0.177
*/